import flask
from flask_api import FlaskAPI
from flask import Request
from flask import jsonify
from typing import Optional
import mysql.connector

db = mysql.connector.connect(
    user="website",
    passwd="secret",
    host='127.0.0.1',
    database="webdb")

cur = db.cursor(dictionary=True, buffered=True)

app = flask.Flask(__name__)

@app.route('/')
def index():
    return Response('index')

@app.route('/api/inventory/', methods=['GET'])
def get_all_items():
    """
    Args:
    Returns:
    """

    cur.execute("SELECT * FROM Items")

    ret = [row for row in cur]
    return jsonify(ret)


@app.route('/api/inventory/<string:item_id>/', methods=['GET', 'POST'])
def get_single_item(item_id):
    """
    Args: item id of item to retrieve
    Returns: item object retrieved
    """

    if Request.method == 'POST':
        req = Request.data
        cur.execute(""" INSERT INTO Items (ITEM_ID, CHECKED_OUT, STATUS, PANTHER_ID)
        VALUES
        ({})""".format(req.get('item_id'), req.get('checked_out'), req.get('status'), req.get('panther_id')))
        return 
if __name__ == "__main__":
    app.run(debug=True)
