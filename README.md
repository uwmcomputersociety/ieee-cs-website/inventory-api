# README

## Endpoints:

GET - Returns array of all items and their respective attribute fields
> /api/inventory/

GET -  Returns array of item fields with specified ID including checked_out, 
status, and the pather_id of the lendee (null if checked_out is false)
> /api/inventory/item_id

GET - Returns array of items checked out by member id
> /api/inventory/member/member_id

POST - Adds item to inventory and Returns array of item fields that are added to inventory
> /api/inventory/item_id



